using System.Data.Entity.Migrations;
using System.Linq;
using DemoWeb.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DemoWeb.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            if (!context.Roles.Any(r => r.Name == UsersType.Admin.ToString()))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = UsersType.Admin.ToString() };

                manager.Create(role);
            }

            if (!context.Users.Any(u => u.UserName == "founder@w.p"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser { UserName = "founder@w.p", Email = "founder@w.p" };

                manager.Create(user, "ChangeItAsap!");
                manager.AddToRole(user.Id, UsersType.Admin.ToString());
            }

            if (!context.Roles.Any(r => r.Name == UsersType.User.ToString()))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = UsersType.User.ToString() };

                manager.Create(role);
            }
        }
    }
}
