﻿using System.Threading.Tasks;
using System.Web.Mvc;
using DemoWeb.Model.Dto;
using DemoWeb.Model.Interfaces;
using DemoWeb.Model.Services;

namespace DemoWeb.Controllers
{
    [Authorize(Roles =  "Admin")]
    public class ProductManagementController : Controller
    {
        private readonly IProductService _productService;

        public ProductManagementController()
        {
            _productService = new ProductService();
        }
        // GET: ProductManagement
        public async Task<ActionResult> Index()
        {
            var products = await _productService.GetAllProduct();
            return View(products);
        }

        [HttpGet]
        public ActionResult AddProduct()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AddProduct(AddProductDto model)
        {
            if (ModelState.IsValid)
            {
                var product = await _productService.AddProduct(model);

                if (product != null)
                {
                    return RedirectToAction("Index");
                }
            }

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> EditProduct(int productId)
        {
            var product = await _productService.GetProductById(productId);

            return View(product);
        }

        [HttpPost]
        public async Task<ActionResult> EditProduct(ProductDto product, int productId)
        {
            if (ModelState.IsValid)
            {
                if (await _productService.EditProduct(product, productId))
                {
                    return RedirectToAction("Index");
                }

                 return View(product);
            }

            return View(product);
        }

        public async Task<ActionResult> DeleteProduct( int productId)
        {
            if (ModelState.IsValid)
            {
                if (await _productService.DeleteProduct(productId))
                {
                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("Index");
        }
    }
}