﻿using System.Threading.Tasks;
using System.Web.Mvc;
using DemoWeb.Model.Dto;
using DemoWeb.Model.Interfaces;
using DemoWeb.Model.Services;

namespace DemoWeb.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductService _productService;

        public ProductController()
        {
            _productService = new ProductService();
        }
        // GET: Product
        public async Task<ActionResult> Index()
        {
            var model = await _productService.GetAllProduct();

            return View(model);
        }

        public async Task<ActionResult> ProductDetails(int productId, ProductDto mDto)
        {
            var model = await _productService.GetProductById(productId);

            return View(model);
        }
    }
}