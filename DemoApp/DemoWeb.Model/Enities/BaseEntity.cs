﻿using System.ComponentModel.DataAnnotations;

namespace DemoWeb.Model.Enities
{
    public class BaseEntity
    {
        [Key]
        public int Id { get; set; }
    }
}
