﻿using System.ComponentModel.DataAnnotations;

namespace DemoWeb.Model.Enities
{
    public class Product : BaseEntity
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        public decimal Price { get; set; }
        public string LinkToPicture { get; set; }
    }
}
