﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DemoWeb.Model.Dto;

namespace DemoWeb.Model.Interfaces
{
    public interface IProductService
    {
        Task<List<ProductDto>> GetAllProduct();
        Task<ProductDto> GetProductById(int productId);
        Task<ProductDto> AddProduct(AddProductDto productDto);
        Task<bool> EditProduct(ProductDto productDto, int idProduct);
        Task<bool> DeleteProduct(int idProduct);
    }
}
