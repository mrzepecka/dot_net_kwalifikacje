﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using DemoWeb.Model.Dto;
using DemoWeb.Model.Enities;
using DemoWeb.Model.Interfaces;

namespace DemoWeb.Model.Services
{
    public class ProductService: IProductService
    {
        private readonly ModelContext _context;

        public ProductService()
        {
            _context = new ModelContext();
        }

        public async Task<List<ProductDto>> GetAllProduct()
        {
            var products = _context.Set<Product>() as IQueryable<Product>;

            return await products.Select(prod => new ProductDto
            {
                Id = prod.Id,
                Description = prod.Description,
                LinkToPicture = prod.LinkToPicture,
                Name = prod.Name,
                Price = prod.Price
            }).ToListAsync();
        }

        public async Task<ProductDto> GetProductById(int productId)
        {
            return ConstrucProductDto(await GetProductEntitiesById(productId));
        }

        public async Task<ProductDto> AddProduct(AddProductDto productDtoDto)
        {
            var product = new Product
            {
                Price = productDtoDto.Price,
                Description = productDtoDto.Description,
                Name = productDtoDto.Name,
                LinkToPicture = productDtoDto.LinkToPicture
            };

            _context.Products.Add(product);
            await _context.SaveChangesAsync();

            return ConstrucProductDto(product);
        }

        public async Task<bool> EditProduct(ProductDto productDto, int idProduct)
        {
            var product = await GetProductEntitiesById(idProduct);

            if (product == null)
            {
                return false;
            }

            product.Description = productDto.Description;
            product.LinkToPicture = productDto.LinkToPicture;
            product.Name = productDto.Name;
            product.Price = productDto.Price;

            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<bool> DeleteProduct(int idProduct)
        {
            var product = await GetProductEntitiesById(idProduct);

            if (product == null)
            {
                return false;
            }

            _context.Products.Remove(product);
            return await _context.SaveChangesAsync() > 0;
        }

        private static ProductDto ConstrucProductDto(Product product)
        {
            return new ProductDto()
            {
                Id = product.Id,
                Price = product.Price,
                Description = product.Description,
                Name = product.Name,
                LinkToPicture = product.LinkToPicture
            };
        }
        private async Task<Product> GetProductEntitiesById(int id)
        {
            return await _context.Products.SingleOrDefaultAsync(x => x.Id == id);
        }
    }
}
