namespace DemoWeb.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelDbContext : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(),
                        Prise = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LinkToPicture = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Products");
        }
    }
}
