using DemoWeb.Model.Enities;

namespace DemoWeb.Model.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<ModelContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ModelContext context)
        {
            context.Products.AddOrUpdate(
                new Product { Name = "Pencils", Description = "24 colored pencils in a metal container.", Price = 15.5m, LinkToPicture = "https://www.bigw.com.au/medias/sys_master/images/images/h18/haa/10668781436958.jpg" },
                new Product { Name = "Pencils", Description = "12 colored pencils in a metal container.", Price = 12.5m, LinkToPicture = "https://cdn3.volusion.com/4jdsg.2wu4y/v/vspfiles/photos/606103-2.jpg?1413799451" },
                new Product { Name = "Crayons", Description = "12 colored crayons", Price = 5.0m, LinkToPicture = "https://images-na.ssl-images-amazon.com/images/I/51moQ3ROy2L._SL500_AC_SS350_.jpg" },
                new Product { Name = "Pen", Description = "Two blue pens. Metal housing. Blue refill.", Price = 26m, LinkToPicture = "https://cdn3.volusion.com/mwceg.gjtbh/v/vspfiles/photos/POTUS-EAGLESET-3.jpg?1527266940" },
                new Product { Name = "Notebook", Description = "100 sheets, hard cover", Price = 16.99m, LinkToPicture = "https://sc02.alicdn.com/kf/HTB13rJcahHBK1JjSZFu760RSpXaZ/Free-sample-exercise-cheap-bulk-notebooks-notebook.png" }
            );
        }
    }
}