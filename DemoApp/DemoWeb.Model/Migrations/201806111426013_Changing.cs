namespace DemoWeb.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Changing : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.Products", "Prise");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Products", "Prise", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.Products", "Price");
        }
    }
}
