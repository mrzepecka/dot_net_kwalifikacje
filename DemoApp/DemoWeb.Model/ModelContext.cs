﻿using System.Data.Entity;
using DemoWeb.Model.Enities;

namespace DemoWeb.Model
{
	public class ModelContext : DbContext
	{
		public ModelContext() : base("DefaultConnection")
		{
			Configuration.LazyLoadingEnabled = false;
		}
	    public DbSet<Product> Products { get; set; }
    }
}
