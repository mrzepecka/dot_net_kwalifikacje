﻿using System.ComponentModel.DataAnnotations;

namespace DemoWeb.Model.Dto
{
    public class AddProductDto
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal Price { get; set; }
        public string LinkToPicture { get; set; }
    }
}
