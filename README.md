## Zadanie rekrutacyjne SI-Consulting

1. Zrób fork-a tego repozytorium.
2. Zaimplementuj moduł produktów dla sklepu internetowego.
3. Zrób pull-request z opisem wykonanych zmian.

## Jak oceniamy?

1. Przede wszystkim liczy się dla nas przejrzystość kodu źródłowego i idea implementowanego rozwiązania.
2. Zwracamy uwagę na sposób rozwiązania i stosowanie dobrych praktyk.
3. Rozwiązanie nie musi działać w pełni.

## Funkcjonalności jako user story

1. Jako użytkownik (zalogowany lub niezalogowany) chcę wyświetlić wszystkie produkty (sposób wyświetlenia dowolny, może być lista, prosta tabela lub karty).
2. Jako użytkownik (zalogowany lub niezalogowany) chcę wyświetlić podgląd/szczegóły pojedynczego produktu.
3. Jako administrator chcę dodawać produkty.
4. Jako administrator chcę edytować produkty.
5. Jako administrator chcę usuwać produkty.

## Wymogi

1. Model produktu musi posiadać pola: id, nazwę, opis, cenę.
2. Model dodatkowo może posiadać zdjęcie produktu.
3. Cena może być prezentowana jako złotówki.
4. Wprowadź obsługę roli Admin.

## Wskazówki

1. Staraj się używać Bootstrap, aby moduł produktów był spójny graficznie ze stroną.
2. Stosuj dobre praktykti programowania.
3. Staraj się używać nazw anglojęzycznych.
4. Logikę biznesową zaimplementuj w projekcie Model.
5. Zastosuj migrację do zaktualizowania bazy danych.
6. Możesz uzupełnić bazę danych wstępnymi danymi (w pliku Configuration w metodzie Seed).